import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.*;
import javafx.scene.canvas.*;
import javafx.scene.input.*;
import javafx.stage.*;
import pfx.FXApp;
import logging.*;

public class Main extends Application {
    
    Canvas c;
    FXApp app;
    FXApp defaultApp;

    boolean scaling = true;
    boolean showPerformance;

    private Rectangle2D bounds;

    @Override
    public void start(Stage stage) throws Exception{

    //gets boundaries of the screen (does not work right)
        bounds = Screen.getPrimary().getVisualBounds();
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());

	//A group of images?, resizes based on num of children
        Group root = new Group();
        Scene scene = new Scene(root);//grab frame buffer
        stage.setScene(scene);
        stage.setFullScreen(true);
        
	//create canvas
	    c = new Canvas(bounds.getWidth(), bounds.getHeight());

    //adds canvas to frame buffer
        root.getChildren().add(c);

	//set up default application
	    defaultApp = new ApplicationChooser(c.getGraphicsContext2D(), (FXApp a) -> {
	        app = a;
	        app.settings();
	        app.setup();
	        app.fxRender(scaleGraphics);
	    });

        app = defaultApp;
        app.settings();
        app.setup();

        scene.addEventHandler(KeyEvent.KEY_PRESSED, (evt) -> {
            if (evt.getCode() == KeyCode.F4) {
                System.exit(0);
            } else if (evt.getCode() == KeyCode.F5){
                showPerformance = !showPerformance;
            } else if (evt.getCode() == KeyCode.F12){
                scaling = ! scaling;
            } else if (evt.getCode() == KeyCode.ESCAPE){
                app = defaultApp;
                c.getGraphicsContext2D().restore();
            } else if(evt.getCode() == KeyCode.P){
                Logger.getInstance().ifPresent(e -> System.out.println(new LoggerExtractor(e).toString()));
            }
            app.keyPressed(evt);
        });

        scene.addEventHandler(KeyEvent.KEY_TYPED, (evt) -> {
            app.key = evt.getCharacter().charAt(0);
            app.keyTyped();
        });

        new AnimationTimer(){
            public void handle(long currentNanoTime){
                Logger.getInstance().ifPresent(e -> e.endFrame(app));

                c.getGraphicsContext2D().save();
                app.fxDraw(scaleGraphics); //draws the frame
                c.getGraphicsContext2D().restore();

                Logger.getInstance().ifPresent(e -> e.startFrame(app));
            }
        }.start();

        stage.show();
    }

    private Runnable scaleGraphics = () -> {
        if (scaling) {
            c.getGraphicsContext2D().scale(bounds.getWidth() / app.width, bounds.getHeight() / app.height);
        } else {
            double xoffset = (bounds.getWidth() - app.width) / 2;
            double yoffset = (bounds.getHeight() - app.height) / 2;
            c.getGraphicsContext2D().translate(xoffset, yoffset);
        }
    };

    public static void main(String[] args) {
        if(args != null && args.length >= 1)Logger.setEnabled(args[0].equalsIgnoreCase("t"));
        launch(args);
    }
}
