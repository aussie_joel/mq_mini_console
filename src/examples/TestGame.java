package examples;

import javafx.scene.canvas.GraphicsContext;

public class TestGame extends pfx.FXApp {
    public TestGame(GraphicsContext g) {
        super(g);
    }

    public String name(){return "TestGame by Joel Morrissey";}
    public String description(){return "A test duplicated program";}

    public void settings(){
        size(960,555);
    }

    public void draw(){
        background(0);
        fill(0,0,255);
        stroke(255);
        rect(5,5,950,545);
    }
}
