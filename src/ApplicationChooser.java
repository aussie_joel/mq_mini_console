import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Pair;
import pfx.FXApp;
import studentwork.BoxCarrier;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class ApplicationChooser extends FXApp {

    private int numOfGamesOnRow = 7;
    private int numOfGamesOnCol = 4;
    List<Pair<FXApp,Optional<Image>>> apps;
    Image defaultImage;
    int boxWidth;
    int boxHeight;
    int gapSize;
    int topSpot;
    int selected;
    int numRender = numOfGamesOnRow*numOfGamesOnCol;
    Consumer<FXApp> appResetter;

    public ApplicationChooser(GraphicsContext g, Consumer<FXApp> appResetter) {
        super(g);
        this.appResetter = appResetter;

        apps = Arrays.asList(
                new Pair(new BoxCarrier(g), Optional.empty()),
                new Pair(new examples.BouncingBall(g), Optional.empty()),
                new Pair(new examples.SineWave(g), Optional.empty()),
                new Pair(new examples.ScalingChecker(g), Optional.empty()),
                new Pair(new examples.AdditiveWave(g), Optional.empty()),
                new Pair(new examples.Array2D(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty()),
                new Pair(new examples.Array2D(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty()),
                new Pair(new examples.TestGame(g), Optional.empty())
        );
    }

    @Override
    public void settings()
    {
        size(1920, 1080);
        strokeWeight(1);
    }

    public void setup() {
        boxWidth = height / (numOfGamesOnRow-1);
        boxHeight = height / numOfGamesOnCol;
        gapSize = height / 30;
        selected = 0;
        defaultImage = new Image("boxcarrier.png", boxWidth-2, boxHeight-2, true, false);
    }

    public void draw() {

        background(214, 210, 196);

        noFill();
        /*
        int oGap = width / 60;
        int ycoord = oGap;
        gapSize = oGap;
        for (int i = 0; i < numRender && i < apps.size(); i++) {
            if ((i+1)%(numOfGamesOnRow+1) == 0 && i != 0) {
                gapSize = oGap;
                ycoord += boxHeight + 30; //sets y cord
            }
            drawGamesSelect(i, ycoord);
            gapSize = ((oGap) * ((i+1)%numOfGamesOnRow+1) + (boxWidth * (((i+1)%numOfGamesOnRow))));
            //ycoord = ycoord + boxSize + (height / 30);
            /*
            if (i == selected) {
                stroke(166, 25, 46);
            } else {
                stroke(55, 58, 54);
            }
            */
        //}



        for(int i =0; i < apps.size();i++ ){
            int x = (i % numOfGamesOnRow);
            int y = (i / numOfGamesOnRow);
            drawGamesSelect(x,y,i);
        }

    }


    private void drawGamesSelect(int gridX, int gridY,int idx) {
        int xcoord = gridX * boxWidth + boxWidth;
        int ycoord = gridY * boxHeight + boxHeight;
        if (idx == selected) {
            drawGameIcon(idx, xcoord, ycoord);

            fill(255,0,0);
            ellipse(xcoord+10, ycoord+10, 10, 10);
            noFill();
        } else {
            drawGameIcon(idx, xcoord, ycoord);
        }
        drawGameInfo(idx, xcoord, ycoord, idx == selected);
    }

    private void drawGameIcon(int idx, int xcoord, int ycoord) {
        fill(128,128,128);
        rect(xcoord-5, ycoord-5, boxWidth+10, boxHeight+10);
        noFill();
        fill(0,0,0);
        rect(xcoord, ycoord, boxWidth, boxHeight);
        noFill();
        image(apps.get(idx).getValue().orElse(defaultImage), (xcoord)+1, ycoord+1);
    }

    private void drawGameInfo(int idx,int xcoord ,int ycoord ,boolean tf) {
        fill(255, 255, 255);
        Object[] GameInfo;
        if (tf) {
            GameInfo = cleanUpText(apps.get(idx).getKey().name().replace("\n", ""));
            Object[] temp = cleanUpText(apps.get(idx).getKey().description().replace("\n", ""));
            GameInfo[0] = (String)(GameInfo[0]) + "\n" + (String) (temp[0]);
            GameInfo[1] = (int) (GameInfo[1]) + (int)(temp[1]);
        } else {
            GameInfo = cleanUpText(apps.get(idx).getKey().name());
        }
        String nameOfGame = (String) (GameInfo[0]);
        text(nameOfGame,xcoord, ycoord + boxHeight-((int)(GameInfo[1]))*17);
    }

    public Object[] cleanUpText(String s) {
        Object[] items = new Object[2];
        int numOfNewLines = 0;
        for (int i = 0; i<s.length(); i++) {
            if (i%28 == 0) {
                numOfNewLines++;
                s = s.substring(0, i) + "\n" + s.substring(i);
            }
            /*
            if (i%28 == 0 && i != 0) {
                numOfNewLines++;
                if (s.charAt(i-1) == ' ') {
                    s = s.substring(0, i) + "\n" + s.substring(i);
                } else {
                    for (int j = i; j>=0; j--) {
                        if (j == 0) {
                            s = s.substring(0, i) + "\n" + s.substring(i);
                            break;
                        } else if (s.charAt(j-1) == ' ') {
                            s = s.substring(0, j) + "\n" + s.substring(j);
                            break;
                        }
                    }
                }
            }
            */
        }
        items[0] = s;
        items[1] = numOfNewLines+1;
        return items;
    }

    public void keyPressed(KeyEvent evt) {
        if (evt.getCode() == KeyCode.RIGHT) {
            selected = (selected + 1) % apps.size();
        } else if (evt.getCode() == KeyCode.LEFT) {
            selected = (selected == 0 ? apps.size() - 1 : (selected - 1));
        }else if (evt.getCode() == KeyCode.DOWN) {
            selected = (selected + numOfGamesOnRow > apps.size() ? selected % numOfGamesOnRow : selected + numOfGamesOnRow);
        } else if (evt.getCode() == KeyCode.UP) {
            selected = (selected - numOfGamesOnRow < 0 ? (apps.size()) - ((apps.size()-numOfGamesOnRow)-selected) : selected-numOfGamesOnRow);
        }else if (evt.getCode() == KeyCode.ENTER){
            appResetter.accept(apps.get(selected).getKey());
        }
        /*
        if (evt.getCode() == KeyCode.UP) {
            selected = (selected - 1);
            if (selected < 0)
                selected = apps.size() - 1;
            recalcGlobals();
        } else if (evt.getCode() == KeyCode.DOWN) {
            selected = (selected + 1) % apps.size();
            recalcGlobals();
        } else if (evt.getCode() == KeyCode.ENTER){
            g.save();
            appResetter.accept(apps.get(selected).getKey());
        }
        */
    }
}
