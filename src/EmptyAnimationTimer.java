import javafx.animation.AnimationTimer;
import logging.Logger;

public class EmptyAnimationTimer extends AnimationTimer {

    public EmptyAnimationTimer() {
        super();
    }


    @Override
    public void handle(long now) {
        Logger.getInstance().ifPresent(e -> e.endFrame(this));
        Logger.getInstance().ifPresent(e -> e.startFrame(this));
    }
}
