package logging;

import java.util.*;
import java.util.stream.LongStream;
import java.util.AbstractMap.SimpleEntry;


public class LogStream{

    String name;
    //all of the data
    long[] data;
    LongSummaryStatistics stats;
    float stdDev;

    //subset of non outliers
    List<SimpleEntry<Integer,Long>> prunedData;
    LongSummaryStatistics prunedStats;
    float prunedStdDev;

    //subset of outliers
    List<SimpleEntry<Integer,Long>> outliers;
    LongSummaryStatistics outliersStats;
    float outlierStdDev;


    public LogStream(long[] data,String name){
        this.data = data;
        this.name = name;
        this.stats = LongStream.of(data).summaryStatistics();
        this.stdDev = (float) getStdDev(Arrays.stream(data),stats.getAverage());//(float)Math.sqrt(Arrays.stream(data)
        prunedData = new ArrayList<>();
        outliers = new ArrayList<>();

       for(int i =0; i < data.length; i++) {
           (isOutlier(getZValue(data[i]))? outliers : prunedData)
                   .add(new SimpleEntry<>(i,data[i]));
       }

       prunedStats = prunedData.stream()
               .mapToLong(SimpleEntry::getValue)
               .summaryStatistics();

       prunedStdDev = (float) getStdDev(prunedData.stream()
                       .mapToLong(SimpleEntry::getValue)
               ,prunedStats.getAverage());

       outliersStats = outliers.stream()
               .mapToLong(SimpleEntry::getValue)
               .summaryStatistics();

       outlierStdDev = (float) getStdDev(outliers.stream()
                       .mapToLong(SimpleEntry::getValue)
                ,prunedStats.getAverage());
    }

    private String serializeSubSet(Collection<SimpleEntry<Integer,Long>> data
            , LongSummaryStatistics stats
            , float stdDev){
        return " N: " + stats.getCount()           + '\n' +
                " Max: " + stats.getMax()        + " Ms" + '\n' +
                " Min: " + stats.getMin()        + " Ms" + '\n' +
                " Mean: " + stats.getAverage()   + " Ms" + '\n' +
                " Std Dev: " + stdDev            + " Ms" + '\n' +
                " Data(FrameNo/Ms): " + data.toString()    + '\n';
    }

    @Override
    public String toString(){
        return "Name: " + name +"\n\n" +
                " Raw Data: " +
                " Frames: " + stats.getCount()           + '\n' +
                " Max: " + stats.getMax()        + " Ms" + '\n' +
                " Min: " + stats.getMin()        + " Ms" + '\n' +
                " Mean: " + stats.getAverage()   + " Ms" + '\n' +
                " Std Dev: " + stdDev            + " Ms" + '\n' +
                " Data(Ms): " + Arrays.toString(data)    + '\n' +
                "\n Pruned Data: \n" +
                serializeSubSet(prunedData,prunedStats,prunedStdDev) + '\n' +
                " Outliers: \n"    +
                serializeSubSet(outliers,outliersStats,outlierStdDev) + '\n';
    }

    private double getStdDev(LongStream c, double average){
        return Math.sqrt(c.mapToDouble(e -> Math.pow(e-average,2) )
                .summaryStatistics().getAverage());
    }

    private float getZValue(long value){
        return (float)Math.abs((value - stats.getAverage()) / stdDev);
    }

    private boolean isOutlier(float zValue){
        return (zValue >= 4);
    }
}
