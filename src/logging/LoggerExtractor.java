package logging;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class LoggerExtractor {

    private List<LogStream> data;


    public LoggerExtractor(Logger logger){
        data = new ArrayList<>();
        for(Map.Entry<Integer, List<Pair<Long,Long>>> m : logger.dataOut().entrySet()) {
            data.add(new LogStream(m.getValue()
                    .stream()
                    .filter(e -> e != null && e.getValue() != -1L)
                    .mapToLong(e -> (e.getValue() - e.getKey()))
                    .toArray(), logger.idxToName(m.getKey())));
        }
    }

    @Override
    public String toString() {
        String string = "";

        for(LogStream l : data) string += l.toString();

        return string;
    }
}
