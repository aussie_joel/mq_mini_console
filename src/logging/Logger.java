
package logging;

import javafx.util.Pair;

import java.util.*;

public class Logger {
	private static Logger instance = null;
	private static boolean isEnabled = true;

	private HashMap <Integer, List<Pair<Long,Long>>> data = new HashMap <>();
	private HashMap <Integer, String> progName = new HashMap <>();

	public static void setEnabled(boolean enabled) {
		isEnabled = enabled;
	}
	
	private Logger() {}
	
	public static Optional<Logger> getInstance() {
		if (isEnabled && instance == null) {
			instance = new Logger();
		}
		return(isEnabled)? Optional.of(instance) : Optional.empty();
	}

	public void startFrame(Object obj){
		if (!data.containsKey(obj.hashCode())) addNewObject(obj);

		data.get(obj.hashCode()).add(new Pair<Long,Long>(System.currentTimeMillis(),-1L));

	}

	public void endFrame(Object obj){
		if (!data.containsKey(obj.hashCode())) return;

		List<Pair<Long,Long>> l = data.get(obj.hashCode());
		Long start = l.get(l.size() -1).getKey();
		l.set(l.size() -1 , new Pair<>(start,System.currentTimeMillis()));

	}

	private void addNewObject(Object o){
		ArrayList<Pair<Long,Long>> a = new ArrayList<>();
		data.put(o.hashCode(), a);
		progName.put(o.hashCode(),o.getClass().getName());
	}

	public String idxToName(int idx) {
		return progName.get(idx);
	}
	
	public HashMap<Integer, List<Pair<Long,Long>>> dataOut() {
		return data;
	}
	
}
